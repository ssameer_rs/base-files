
set IMAGE_TAG =%1
set APPLICATION_NAME=component_name
set IMAGE_NAME=rs_%APPLICATION_NAME%
set REGISTERGY_NAME=ssameer03
set REPO_IMAGE=%REGISTERGY_NAME%/%IMAGE_NAME%:%IMAGE_TAG%
echo "> Cleaning target..."
rm -Rf target
echo "> building jar..."
call mvn package -Dmaven.test.skip=true

echo "> Building %APPLICATION_NAME%:latest"
docker build -t %IMAGE_NAME%:%IMAGE_TAG% .

docker tag %IMAGE_NAME% %REPO_IMAGE%

docker push %REPO_IMAGE%
echo "> Done."

